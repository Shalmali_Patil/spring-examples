# README #

This repository contains code samples showcasing *Spring Framework* features. I have used Spring Boot for all projects for faster and hassle-free development. Following are the features covered till now :

1. Spring Data using Spring Boot


### How do I get set up? ###

* Summary of set up 

  Since Spring Boot along with Maven has been used for application development, building and deployment becomes easy. Spring Boot has an embedded tomcat (Default port:8080), so building and deployment can be done in a single step. Please read steps mentioned below for the code setup.

* Configuration

Software/Tool  | Version
------------- | -------------
Java  | JDK 1.8
Apache Maven  | 3.0.4
MySQL | 5.5

* Database configuration 

1. Database dump file is checked in along with the code. File location : spring-data-basic/src/main/resources/db_dumps/myDatabase.sql
2. Source the above mentioned file before starting the application.
3. Set/change the DB properties (like DB user name-password, database name etc.) in application.properties file at the location : <project location>/src/main/resources/

* Deployment instructions
Following command builds the code and takes care of deploying it in the embedded tomcat as well : 
mvn spring-boot:run
Once deployment is successful, it should be available at http://localhost:8080/ (or custom port if set in application.properties)