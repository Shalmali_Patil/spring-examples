package com.techneophilia.webservice;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.techneophilia.model.Employee;
import com.techneophilia.repo.EmployeeRepository;

@RestController
@RequestMapping("/employee")
public class EmployeeResource {

	@Autowired
	EmployeeRepository employeeRepository;

	@RequestMapping("/hello")
	public Map<String, String> hello(@RequestParam("name") String name) {
		Map<String, String> responseMap = new HashMap<String, String>();
		responseMap.put("message", "Hello, " + name);
		return responseMap;
	}

	@RequestMapping(value = "/{employeeId}", method = RequestMethod.GET)
	public Employee getEmployeeById(@PathVariable Integer employeeId) {
		Employee emp = null;
		if (employeeId != null && employeeId > 0) {
			emp = employeeRepository.findOne(employeeId);
		}
		return emp;
	}

	@RequestMapping(method = RequestMethod.POST)
	public Employee createEmployee(@RequestBody Employee employee) {
		Employee createdEmployee = employeeRepository.save(employee);
		return createdEmployee;
	}

	@RequestMapping(value = "/{employeeId}", method = RequestMethod.PUT)
	public Employee updateEmployee(@RequestBody Employee employee,
			@PathVariable Integer employeeId) {
		employee.setId(employeeId);
		Employee createdEmployee = employeeRepository.save(employee);
		return createdEmployee;
	}

	@RequestMapping(value = "/{employeeId}", method = RequestMethod.DELETE)
	public Map<String, String> deleteEmployee(@PathVariable Integer employeeId) {
		Map<String, String> responseMap = new HashMap<String, String>();
		employeeRepository.delete(employeeId);
		responseMap.put("message", "Record deleted.");
		return responseMap;
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public Page<Employee> search(@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo, 
			@RequestParam(value = "pageSize", required = false, defaultValue = "10" ) Integer pageSize,
			@RequestParam(value = "name", required = false, defaultValue = "") String name) {
		// page no. in PageRequest is 0 based.
		PageRequest pageRequest = new PageRequest(pageNo-1, pageSize, new Sort(Sort.Direction.ASC, "id"));
		Page<Employee> result = employeeRepository.findByNameLike("%"+name+"%", pageRequest);
		return result;
	}
}
