package com.techneophilia.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.techneophilia.model.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Integer>/*, JpaSpecificationExecutor<Employee>*/{
	Page<Employee> findByNameLike(String name, Pageable pageRequest);
}
